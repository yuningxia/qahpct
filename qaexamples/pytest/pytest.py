# SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: BSD-3-Clause

import time
x = {}
for i in range(10**7):
  x[i % 6] = i
def g():
  time.sleep(3)
def f():
  g()
  time.sleep(2)
f()
