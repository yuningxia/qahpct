#!/bin/bash

# SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: BSD-3-Clause

###################################################################
#  Start main code here

suitemode=$1

if [ "$2" = "" ] ; then
	echo "ERROR: suite.level0 requires the second argument to be the target"
	exit 1
else
	OTARGET=$2
fi
#  Bring in the subroutine to run the suite
source $QAHPCT_ROOT/bin/subs/dosuite

/bin/rm -rf LOG.suite.level0
# module use /usr/local/modules

# clean everything out
make clean > /dev/null

echo "Starting suite.level0, ${USER}@`hostname` on `date`" | tee -a LOG.suite.level0
echo "       in directory `pwd` " | tee -a LOG.suite.level0

#  Figure out whether to use level0 or cpu test script
#  If the target has "nooffset" in the name, it is NOT level0-enabled
if [ "`echo ${OTARGET} | grep nooffload `" = "" ]; then
	# The target is a level0 enabled test
	stype="level0"
	echo "   ${OTARGET} is level0-enabled, running $stype tests" | tee -a LOG.suite.level0
else
	# The target is not level0-enabled; run cpu tests
	stype="cpu"
	echo "   ${OTARGET} is NOT level0-enabled; running $stype tests" | tee -a LOG.suite.level0
fi

if [ "${LEVEL0_VERSIONS}" = "" ]; then
	###########################################################
	# Path taken for single-version (loaded module) run
	#
	# determine the TARGET_SUFFIX to use, based on the current LEVEL0_PATH
	# echo "LEVEL0_PATH = ${LEVEL0_PATH}"

	###### -> this may casue basename to crap out, if #{LEVEL0_PATH} is NULL
	# base=`basename ${LEVEL0_PATH}`
	# echo "base = ${base}"
	# export level0_vers=`basename ${LEVEL0_PATH} | sed 's/level0//' | sed 's/-//'`

	# echo "LEVEL0_VERSIONS is not specified" | tee -a LOG.suite.level0
	export level0_vers="`icpx --version 2>&1 | head -1 | cut -d " " -f5`"
	echo "  No level0 version list given; using loaded level0 = ${level0_vers}" | tee -a LOG.suite.level0
	export TARGET_SUFFIX=".-${level0_vers}"

	dosuite $suitemode $stype LOG.suite.level0

else
	###########################################################
	# Path taken if the set of versions to loop through is specified
	#
	# find the set of level0 versions to try
	echo "Importing LEVEL0_VERSIONS = ${LEVEL0_VERSIONS}" | tee -a LOG.suite.level0
	echo ""  | tee -a LOG.suite.level0

	declare -a arr_level0
	for i in  $(echo ${LEVEL0_VERSIONS} ); do arr_level0+=($i); done
	
	for level0_vers in "${arr_level0[@]}"
	do
		# run the tests against the currently specified level0 version
		echo "  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  ="  | tee -a LOG.suite.level0
		echo "  --  Loading level0/${level0_vers}"  | tee -a LOG.suite.level0
		echo "  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  =  ="  | tee -a LOG.suite.level0
		module unload level0
		module load level0/${level0_vers}
		export level0_vers
		export TARGET_SUFFIX=".-${level0_vers}"
	
		# invoke the proper test script
		dosuite $suitemode $stype LOG.suite.level0
	done
fi

echo "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #" | tee -a LOG.suite.level0
echo "  End of suite.level0 run in directory `pwd` "  | tee -a LOG.suite.level0
echo "        Current files:"  >> LOG.suite.level0
ls -lF . >> LOG.suite.level0
echo "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #" | tee -a LOG.suite.level0
