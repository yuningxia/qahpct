#!/bin/bash

# SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: BSD-3-Clause

###################################################################################
#  Start main code here

#  Bring in the subroutine to run the suite
source $QAHPCT_ROOT/bin/subs/dosuite

suitemode=$1

if [ "$2" = "" ] ; then
	echo "ERROR: suite.mpilevel0 requires the second argument to be the target"
	exit 1
else
	OTARGET=$2
fi

/bin/rm -rf LOG.suite.mpilevel0

# clean everything out
make clean > /dev/null

echo "Starting suite.mpilevel0, ${USER}@`hostname` on `date`" | tee -a LOG.suite.mpilevel0
echo "       in directory `pwd` " | tee -a LOG.suite.mpilevel0

#  Figure out whether to use mpilevel0 or mpicpu test script
#  If the target does not have "nooffload" in the name, it is level0-enabled
if [ "`echo ${OTARGET} | grep nooffload `" = "" ]; then 
        # The target is an Level0 enabled test
        echo "   ${OTARGET} is level0-enabled" | tee -a LOG.suite.mpilevel0
        stype="mpilevel0"
else
        # The target is not level0-enabled; run cpu tests
        echo "   ${OTARGET} is NOT level0-enabled; running MPI CPU tests" | tee -a LOG.suite.mpilevel0
        stype="mpicpu"
fi

if [ "${LEVEL0_VERSIONS}" = "" ]; then
	###########################################################
	# Path taken for single-version (loaded module) run
	#
	# determine the TARGET_SUFFIX to use, based on the current LEVEL0_PATH
	# echo "LEVEL0_PATH = ${LEVEL0_PATH}"

	###### -> this may casue basename to crap out, if #{LEVEL0_PATH} is NULL
	# base=`basename ${LEVEL0_PATH}`
	# echo "OTARGET = ${OTARGET}"
	# export level0_vers=`basename ${LEVEL0_PATH} | sed 's/level0//' | sed 's/-//'`

	# echo "LEVEL0_VERSIONS is not specified" | tee -a LOG.suite.mpilevel0
	export level0_vers="`icpx --version 2>&1 | head -1 | cut -d " " -f5`"
	echo "  No version list given; using loaded level0 = ${level0_vers}" | tee -a LOG.suite.mpilevel0
	export TARGET_SUFFIX=".-${level0_vers}.mpi"

	# invoke the proper test script
        dosuite $suitemode $stype LOG.suite.mpilevel0

else
	###########################################################
	# Path taken if the set of versions to loop through is specified
	#
	# find the set of level0 versions to try
	echo "Importing LEVEL0_VERSIONS = ${LEVEL0_VERSIONS}" | tee -a LOG.suite.mpilevel0

	declare -a arr_level0
	for i in  $(echo ${LEVEL0_VERSIONS} ); do arr_level0+=($i); done
	
	for level0_vers in "${arr_level0[@]}"
	do
		echo "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # " | tee -a LOG.suite.mpilevel0
		# run the tests against the currently specified level0 version
		echo "  --  Loading level0/${level0_vers}"  | tee -a LOG.suite.mpilevel0
		module unload level0
		module load level0/${level0_vers}
		export level0_vers
		export TARGET_SUFFIX=".-${level0_vers}.mpi"
	
		dosuite $suitemode $stype LOG.suite.mpilevel0
	
		echo ""  | tee -a LOG.suite.mpilevel0
	done
fi

echo "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #" | tee -a LOG.suite.mpilevel0
echo "  End of suite.mpilevel0 run in directory `pwd` "  | tee -a LOG.suite.mpilevel0
echo "        Current files:"  >> LOG.suite.mpilevel0
ls -lF . >> LOG.suite.mpilevel0
echo "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #" | tee -a LOG.suite.mpilevel0
