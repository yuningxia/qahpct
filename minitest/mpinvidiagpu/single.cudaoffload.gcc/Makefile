# SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: BSD-3-Clause

SRCS	= ../../src/single.cc ../../src/cudagpu.cu
TARGET	= single.cudaoffload.gcc.cudagpu${TARGET_SUFFIX}

default: $(TARGET)

CUDACXX = nvcc
CUDACXXFLAGS = -allow-unsupported-compiler -g -lineinfo -O2 -std=c++11

MPICXX = g++
OMPFLAGS = -g -O2 -fopenmp -lm

LDFLAGS = -lm

singlempi.o: ../../src/single.cc
	$(MPICXX) -D USE_MPI -I$(MPI_HOME)/include $(OMPFLAGS) -c -o singlempi.o  ../../src/single.cc

cudagpu.o: ../../src/cudagpu.cu
	$(CUDACXX) $(CUDACXXFLAGS) -c -o cudagpu.o ../../src/cudagpu.cu

$(TARGET): singlempi.o cudagpu.o
	$(CUDACXX) -ccbin $(MPICXX) -Xcompiler=-fopenmp singlempi.o cudagpu.o -o $(TARGET) ${LDFLAGS} -L$(MPI_HOME)/lib -lmpi
	@echo ""

include ../../maketargets/mpinvidia.targets
