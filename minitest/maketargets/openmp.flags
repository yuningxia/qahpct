# SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: BSD-3-Clause

# -*- mode: makefile-gmake -*-

#######################################################################
# Set up list delimiters
null :=
space := $(null) #
comma := ,

# Functions to convert between list delimiters
csl = $(subst $(space),$(comma),$(strip $(1)))
ssl = $(subst $(comma),$(space),$(strip $(1)))

# GCC flags
ifeq ($(COMPILER),gcc)
  OMPFLAGS = -fopenmp
  ARCHLIST = $(call ssl,$(ARCH))
  ifeq ("sm","$(findstring sm,$(ARCH))")
    ARCHFLAGS = $(foreach arch,$(ARCHLIST),-foffload=nvptx-none="-misa=sm_$(arch)")
  else ifeq ("gfx","$(findstring gfx,$(ARCH))")
    ARCHFLAGS = $(foreach arch,$(ARCHLIST),-foffload=amdgcn-amdhsa="-march=$(arch)")
  endif
  OMPTGTFLAGS = -foffload-options="-O2 -lm" $(ARCHFLAGS)
endif

ifeq ($(COMPILER),$(MPICXX))
  OMPFLAGS = -fopenmp
  ifeq ($(GPU),nvidia)
    ARCHFLAGS = -foffload=nvptx-none="-misa=$(call csl,$(NVGPUARCH))"
  endif
  ifeq ($(GPU),amd)
    ARCHFLAGS = -foffload=amdgcn-amdhsa="-march=$(call csl,$(AMDGPUARCH))"
  endif
  OMPTGTFLAGS = -foffload-options="-O2 -lm" $(ARCHFLAGS)
endif

# NVIDIA HPC SDK flags
ifeq ($(COMPILER),nvhpc)
  OMPFLAGS = -mp
  ARCHLIST = $(foreach arch,$(call ssl,$(NVGPUARCH)),cc$(arch))
  ARCHFLAGS = -gpu=$(call csl,$(ARCHLIST))
  OMPTGTFLAGS = -mp=gpu $(ARCHFLAGS)
endif

# AMD LLVM flags
ifeq ($(COMPILER),amdllvm)
  OMPFLAGS = -fopenmp

# 5.4.3
#  AMDGPUARCHLIST = $(call ssl,$(AMDGPUARCH))
#  ARCHFLAGS = $(foreach arch,$(AMDGPUARCHLIST),-Xopenmp-target=amdgcn-amd-amdhsa -march=$(arch))
#  OMPTGTFLAGS = -fopenmp-targets=amdgcn-amd-amdhsa $(ARCHFLAGS)

# 5.5.x
#  AMDGPUARCHLIST = $(call ssl,$(AMDGPUARCH))
#  ARCHFLAGS = $(foreach arch,$(AMDGPUARCHLIST),--offload-arch=$(arch))
#  OMPTGTFLAGS = $(ARCHFLAGS)

# 5.6.1
  ARCHFLAGS = --offload-arch=$(call csl,$(AMDGPUARCH))
  OMPTGTFLAGS = $(ARCHFLAGS)

endif

# Cray flags
ifeq ($(COMPILER),craycc)
  OMPFLAGS = -fopenmp
  ifeq ($(GPU),amd)
    AMDGPUARCHLIST = $(call ssl,$(AMDGPUARCH))
    ARCHFLAGS = $(foreach arch,$(AMDGPUARCHLIST),-Xopenmp-target=amdgcn-amd-amdhsa -march=$(arch))
  endif
  OMPTGTFLAGS = -fopenmp-targets=amdgcn-amd-amdhsa $(ARCHFLAGS)
endif

# Intel flags
ifeq ($(COMPILER),intel)
  OMPFLAGS = -qopenmp
  OMPTGTFLAGS = -fopenmp-targets=spir64
endif
