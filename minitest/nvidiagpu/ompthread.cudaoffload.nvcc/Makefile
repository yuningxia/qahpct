# SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: BSD-3-Clause

SRCS	= ../../src/ompthreads.cc ../../src/cudagpu.cu
TARGET	= ompthread.cudaoffload.nvcc.cudagpu${TARGET_SUFFIX}

default: $(TARGET)

CUDACXX = nvcc
CUDACXXFLAGS = -allow-unsupported-compiler -g -lineinfo -O2 -std=c++11

CXX = nvc++
OMPFLAGS = -g -O2 -mp -lm

LDFLAGS = -lm

ompthreads.o: ../../src/ompthreads.cc  ../../src/maincommon.cc
	$(CXX) $(OMPFLAGS) -c -o ompthreads.o  ../../src/ompthreads.cc

cudagpu.o: ../../src/cudagpu.cu
	$(CUDACXX) -ccbin $(CXX) $(CUDACXXFLAGS) -c -o cudagpu.o ../../src/cudagpu.cu

$(TARGET): ompthreads.o cudagpu.o
	@echo "Building ${TARGET}"
	$(CUDACXX) -ccbin $(CXX) -Xcompiler=-fopenmp ompthreads.o cudagpu.o -o $(TARGET) ${LDFLAGS}
	@echo ""

include ../../maketargets/nvidia.targets
