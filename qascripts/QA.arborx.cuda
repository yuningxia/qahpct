#!/bin/bash

# SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: BSD-3-Clause

######################################################################################################################
# QA.arborx.cuda -- script to run QA tests on the arborx.cuda executable
######################################################################################################################
#  The first argument is the suitemode; only "smoke" is currently supported for running tests
#  suitemode clean removes all leftover files and directories from a previous run

# verify QAHPCT_ROOT set
if [ "$QAHPCT_ROOT" = "" ]; then
	echo "QAHPCT module must be loaded to run tests"
	exit 1
fi

export QA_TEST_VARIANT=".cuda"
export suitemode=$1
export targetname=arborx
export time_out="timeout -s6 300"

echo " -----> Start of QA.arborx.cuda on arborx.cuda"
export GPU=cuda
export TARGET=ArborX.cuda/ArborX/build/examples/molecular_dynamics/ArborX_Example_MolecularDynamics.exe
export TARGET_ARGS=""

source $QAHPCT_ROOT/bin/subs/dosuite
runsuite
echo " -----> End of QA.arborx.cuda on arborx.cuda"

echo "End $scriptname $suitemode `date`; exiting $EXITMODE"
exit $EXITMODE
