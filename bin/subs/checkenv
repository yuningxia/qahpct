#!/bin/bash

# SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: BSD-3-Clause

#------------------------------------------------------------
# check the environment for NVIDIA GPUs
#------------------------------------------------------------
checknvgpu()
{
	if [ -x /usr/bin/nvidia-smi ]; then
		minit_hascuda=`nvidia-smi 2>&1 | grep "NVIDIA-SMI has failed" | wc -l`
		minit_hascuda2=`nvidia-smi 2>&1 | grep "No devices were found" | wc -l`
		if [ "$minit_hascuda" = "1" ]; then
		    export minit_hascuda=""
		    # echo "minit No NVIDIA GPU driver found" | tee -a log.minitest
		elif [ "$minit_hascuda2" = "1" ]; then
		    export minit_hascuda=""
		    # echo "minit2: No NVIDIA GPU devices found" | tee -a log.minitest
		else
		    NVGPUARCH=`nvidia-smi --query-gpu=compute_cap --format=csv,noheader | sed 's/^/sm_/; s/\.//g' | tr '\n' ' '`
			echo "checknvgpu: Detected $(echo $NVGPUARCH | wc -w) NVIDIA GPU devices" | tee -a log.minitest
		    # echo "checknvgpu: NVGPUARCH = $NVGPUARCH" | tee -a log.minitest
		    export NVGPUARCH
		    export minit_hascuda=yes
		fi
	else
		export minit_hascuda=""
		# echo "no nvidia-smi: No NVIDIA GPUs found" | tee -a log.minitest
	fi
}

#------------------------------------------------------------
# check the environment for AMD GPUs
#------------------------------------------------------------
checkamdgpu()
{
	minit_hasrocm=`which rocm-smi 2>&1 | grep -v "which: no rocm-smi"`
	if [ "$minit_hasrocm" != "" ]; then
		# rocm-smi -i --showdriverversion  2>&1 | grep -v "=============" | grep -v "^$" | tee -a log.minitest
		# echo -n "  AMD Firmware version: "  | tee -a log.minitest
		# cat /sys/class/kfd/kfd/topology/nodes/*/properties | grep ^fw_version  2>&1 | uniq | tee -a log.minitest
		# rocminfo | grep "gfx" | tee -a log.minitest
		# echo "ROCM_PATH = $ROCM_PATH" | tee -a log.minitest
		AMDGPUARCH=`rocminfo | grep --only-matching 'gfx[0-9a-f]*:sramecc[+-]:xnack[+-]' | sed 's/Name://; s/sramecc[+-]://; s/[[:space:]]*//' | tr '\n' ' '`
		echo "checkamdgpu: Detected $(echo $AMDGPUARCH | wc -w) AMD GPU devices" | tee -a log.minitest
		# echo "checkamdgpu: AMDGPUARCH = $AMDGPUARCH" | tee -a log.minitest
		export AMDGPUARCH
		export minit_hasrocm=yes
	else
		export minit_hasrocm=""
		# echo "No AMD GPUs found" | tee -a log.minitest
	fi
}

#------------------------------------------------------------
# check the environment for Intel GPUs
#------------------------------------------------------------
checkintelgpu()
{
	minit_haslevel0=`which sycl-ls 2>&1 | grep -v "which: no sycl-ls"`
	if [ "$minit_haslevel0" != "" ]; then
		# sycl-ls  | tee -a log.minitest
		export minit_haslevel0=yes
		echo "checkintelgpu: Detected Intel GPU device(s)" | tee -a log.minitest
	else
		export minit_haslevel0=""
		# echo "No Intel GPUs found" | tee -a log.minitest
	fi
}

#------------------------------------------------------------
# check the environment for MPI
#------------------------------------------------------------
checkmpi()
{
	minit_hasmpi="${MPICXX}"
	if [ "$minit_hasmpi" != "" ] ;then 
		# echo "MPICXX = ${MPICXX}" | tee -a log.minitest
		export minit_hasmpi=1
	else
		export minit_hasmpi=""
		minit_hasmpirun="`which mpirun` | grep -v no mpirun"
		minit_hasmpiexec="`which mpiexec` | grep -v no mpiexec"
		if   [ "$minit_hasmpirun" != "" ]; then
			# echo "mpirun = `which mpirun`" | tee -a log.minitest
			export MPILAUNCHER="mpirun"
			export minit_hasmpi=1
		elif [ "$minit_hasmpiexec" != "" ]; then
			# echo "mpiexec = `which mpiexec`" | tee -a log.minitest
			export MPILAUNCHER="mpiexec"
			export minit_hasmpi=1
		fi
		# echo "no mpirun found: = ${minit_hasmpirun}" | tee -a log.minitest
		# echo "No MPI support found" | tee -a log.minitest
	fi

}

#------------------------------------------------------------
# check the environment for GPUs and MPI
#------------------------------------------------------------
checkenv()
{
	#  We always have a CPU
	export minit_hascpu="yes"

	# Check for GPUs
	# echo "XXX checking for Nvidia GPU `date` " | tee -a log.minitest
	# echo "======================== Checking for Nvidia GPUs ============================="  2>&1 | tee -a log.minitest
	checknvgpu
	# echo "======================= End check for Nvidia GPUs ============================="  2>&1 | tee -a log.minitest
	# echo "XXX end checking for Nvidia GPU; start checking for AMD GPU `date` "| tee -a log.minitest
	# echo "======================== Checking for AMD GPUs ================================"  2>&1 | tee -a log.minitest
	checkamdgpu
	# echo "======================= End check for AMD GPUs ============================="  2>&1 | tee -a log.minitest
	# echo "XXX end checking for AMD GPU; start checking for Intel GPU `date` "| tee -a log.minitest
	# echo "======================== Checking for Intel GPUs ================================"  2>&1 | tee -a log.minitest
	checkintelgpu
	# echo "======================= End check for Intel GPUs ============================="  2>&1 | tee -a log.minitest
	# echo "======================= End checks for GPUs ================================"  2>&1 | tee -a log.minitest
	# echo "" | tee -a log.minitest
	
	# echo "======================== Checking for MPI ================================"  2>&1 | tee -a log.minitest
	# echo "XXX end checking for Intel GPU; start checking for MPI `date` "\
	checkmpi
	# echo "======================= End check for MPI ============================="  2>&1 | tee -a log.minitest
 	# echo "======================= End checks of the environmet ================================"  2>&1 | tee -a log.minitest

	# echo "" | tee -a log.minitest
	# echo "XXX end checking for MPI `date` "| tee -a log.minitest

	# Now look for environment variables
	if [ "${MINITEST_NO_REBUILD}" = "1" ]; then
		export minit_no_rebuild=1
		# echo "checkenv: found MINTTEST_NO_REBUILD = ${MINITEST_NO_REBUILD}; rebuilding disabled " | tee -a log.minitest
	else
		export minit_no_rebuild=0
		# echo "checkenv: found MINTTEST_NO_REBUILD = ${MINITEST_NO_REBUILD}; rebuilding will be done " | tee -a log.minitest
	fi
}
