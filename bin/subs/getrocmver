#!/bin/bash

# SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: BSD-3-Clause

getrocmver()
{
	# routine to get the rocm verion; exports ${rcom_ver}
	# try various things to get the version
	# first ROCM_PATH
	if [ "${ROCM_PATH}" != "" ]; then
		rocm_ver_0=`basename ${ROCM_PATH} | sed 's/rocm//' | sed 's/-//'`
	else
		rocm_ver_0=""
	fi

	# then two places from hipcc --version, the installed directory, and the AMD Clang version
	rocm_ver_1="$(hipcc --version | grep InstalledDir | grep -o '[[:digit:]]\+\.[[:digit:]]\+\.[[:digit:]]\+')"
	rocm_ver_2="$(hipcc --version | grep 'AMD clang' | grep -o 'roc-[[:digit:]]\+\.[[:digit:]]\+\.[[:digit:]]\+' | sed 's/roc-//')" 

	# debug print out of the various tries
	if [ "1" = "0" ]; then
	echo " XXX begin debug info" | tee -a LOG.suite.rocm
	echo " XXX rocm_ver_0 (from ROCM_PATH) = \"${rocm_ver_0}\"" | tee -a LOG.suite.rocm
	echo " XXX rocm_ver_1 (from InstalledDir) = \"${rocm_ver_1}\"" | tee -a LOG.suite.rocm
	echo " XXX rocm_ver_2 (from AMD clang line) = \"${rocm_ver_2}\" " | tee -a LOG.suite.rocm
	echo " XXX ROCM_PATH = \"${ROCM_PATH}\"" | tee -a LOG.suite.rocm
	which hipcc | tee -a LOG.suite.rocm
	hipcc --version | tee -a LOG.suite.rocm
	echo " XXX end debug info" | tee -a LOG.suite.rocm
	fi

	# now set the real version
	if [ "${rocm_ver_0}" != "" ]; then
		# version is based on $ROCM_PATH
		export rocm_ver=${rocm_ver_0}
	else
		# no ROCM_PATH resolution, try the current hipcc's installed directory
		if [ "${rocm_ver_1}" != "" ]; then
			# taken from the install directory in hipcc --version,
			#	assuming its path has the three-digit version
			export rocm_ver=${rocm_ver_1}
		else
			# taken from the AMD clang line in the hipcc --version
			export rocm_ver=${rocm_ver_2}
		fi
	fi
}
